const { client } = require('nightwatch-cucumber');
const { Given, Then, When } = require('cucumber');

const browser = client.page.home();

Given(/^I open production Seva Home page$/, async () => await client.url('https://www.seva.id'));

Then(/^I can see Seva Home page$/, async () => await browser.verifyHomePage());

When(/^I click Sign In link at Seva Home page$/, async () => await browser.clickSignInLink());
