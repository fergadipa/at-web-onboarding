const { client } = require('nightwatch-cucumber');
const { Then, When } = require('cucumber');

const login = client.page.login();
const googleLogin = client.page.google_login();
const myAccount = client.page.my_account();

Then(/^I can see Seva Login page$/, async () => await login.verifyLoginPage());

When(/^I input username '([^"]*)' password '([^"]*)' at Seva Login page$/, async (username, password) => {
  await login.inputUsername(username);
  await login.inputPassword(password);
});

When(/^I click submit login button at Seva Login page$/, async () => await login.clickSubmitLoginButton());

Then(/^I can do login successfully for '([^"]*)' at Seva$/, async name => await myAccount.verifyPage(name));

When(/^I click Google login button$/, async () => {
  await login.clickGoogleLoginButton();
});

Then(/^I can see Google Login page$/, async () => {
  await login.switchWindowGoogle();
  await googleLogin.verifyIdentifierPage();
});

When(/^I click Register link at Seva Home page$/, async () => {
  await login.clickRegisterLink();
});
