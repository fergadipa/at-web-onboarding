Feature: Login Seva

@production @loginemail
Scenario: As a customer, I can do general login successfully at production Seva
Given I open production Seva Home page
Then I can see Seva Home page
When I click Sign In link at Seva Home page
Then I can see Seva Login page
When I input username 'customer.satu@yopmail.com' password 'astra123' at Seva Login page
And I click submit login button at Seva Login page
Then I can do login successfully for 'Customer Satu' at Seva