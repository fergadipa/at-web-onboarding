const { client } = require('nightwatch-cucumber');
const base = require('./../base-functions/base-functions.js');

const login = {
  elements: {
    textFieldUsername: '[name=email]',
    textFieldPassword: '[name=password]',
    buttonLogin: '.btn.btn-danger.btn.btn-secondary',
    buttonGoogleLogin: '.ts-button-google',
    linkRegister: '.link-blue.a [href=/register]',
  },
  commands: [{
    async verifyLoginPage() {
      await base.waitElementVisible(this, login.elements.textFieldUsername)
      await base.waitElementVisible(this, login.elements.textFieldPassword)
      await base.waitElementVisible(this, login.elements.buttonLogin)
      await base.waitElementVisible(this, login.elements.buttonGoogleLogin)
      await base.expectVisible(this, login.elements.textFieldUsername)
      await base.expectVisible(this, login.elements.textFieldPassword)
      await base.expectVisible(this, login.elements.buttonLogin)
      await base.expectVisible(this, login.elements.buttonGoogleLogin);
    },
    async inputUsername(username) {
      await base.setValueElement(this, login.elements.textFieldUsername, username);
    },
    async inputPassword(password) {
      await base.setValueElement(this, login.elements.textFieldPassword, password);
    },
    async clickSubmitLoginButton() {
      await base.clickElement(this, login.elements.buttonLogin);
    },
    async clickGoogleLoginButton() {
      await client.waitForElementVisible(login.elements.buttonGoogleLogin, 10000)
        .pause(5000)
        .execute((selector) => {
          document.querySelector(selector).click();
        }, [login.elements.buttonGoogleLogin])
        .moveToElement(login.elements.buttonLogin, 0, 0)
        .click(login.elements.buttonGoogleLogin);
    },
    async clickRegisterLink() {
      await base.clickElement(this, login.commands.linkRegister);
    },
    async switchWindowGoogle() {
      await client.window_handles((result) => {
        const handle = result.value[1];
        client.switchWindow(handle);
      });
    },
  }],
};

module.exports = login;
