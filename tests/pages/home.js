const base = require('./../base-functions/base-functions.js');

const home = {
  elements: {
    signInLink: '.ts-link-signin',
  },
  commands: [{
    async verifyHomePage() {
      await base.waitElementVisible(this, home.elements.signInLink)
      await base.expectVisible(this, home.elements.signInLink);
    },
    async clickSignInLink() {
      await base.clickElement(this, home.elements.signInLink);
    },
  }],
};

module.exports = home;
