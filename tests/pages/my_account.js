const base = require('./../base-functions/base-functions.js');

const myAccount = {
  elements: {
    labelName: '.ts-label-greetings',
  },
  commands: [{
    async verifyPage(name) {
      await base.expectEqualsTextFromElement(this, '@labelName', name);
    },
  }],
};

module.exports = myAccount;
