const timeOut = 10000;

// click element wait for present and visible
const clickElement = function (page, elementSelector) {
  waitElementVisible(page, elementSelector);
  page.click(elementSelector);
};

// wait until element is present
const waitElementPresent = function (page, elementSelector) {
  page.waitForElementPresent(elementSelector, timeOut);
};

// wait until element is visible
var waitElementVisible = function (page, elementSelector) {
  waitElementPresent(page, elementSelector);
  page.waitForElementVisible(elementSelector, timeOut);
};

// set value to element
const setValueElement = function (page, elementSelector, value) {
  waitElementVisible(page, elementSelector);
  page.clearValue(elementSelector, function () {
    page.setValue(elementSelector, value);
  });
};

// set value to element then press ENTER
const setValueElementThenEnter = function (page, elementSelector, value, browser) {
  waitElementVisible(page, elementSelector);
  page.clearValue(elementSelector);
  page.setValue(elementSelector, [value, browser.Keys.ENTER]);
};

// get text from element
var getStringText = function(page, elementSelector, expectedText) {
  waitElementVisible(page, elementSelector);
  return page.getText(elementSelector, function(result) {
    return this.assert.equal(result.value, expectedText);
  });
};

// check if the given element contains the specific text
const assertContainsText = function (page, elementSelector, expectedText) {
  waitElementVisible(page, elementSelector);
  return page.assert.containsText(elementSelector, expectedText);
};

// check if the given element equals the specific text
const expectEqualsTextFromElement = function (page, elementSelector, expectedText) {
  waitElementVisible(page, elementSelector);
  return page.expect.element(elementSelector).text.to.equal(expectedText);
};

var expectEqualsValueFromElement = function(page,elementSelector, expectedValue){
  waitElementVisible(page, elementSelector);
  return page.expect.element(elementSelector).to.have.value.that.equals(expectedValue);
}

// assert page title
const assertPageTitle = function (page, elementSelector) {
  return page.assert.title(elementSelector);
};

// scroll to element
const scrollToElement = function (page, elementSelector) {
  return page.moveToElement(elementSelector, 0, 0);
};

// sleep/pause page
const pauseSleep = function (page, timeSleep) {
  return page.pause(timeSleep);
};

const expectEnabled = function (page,elementSelector){
  return page.expect.element(elementSelector).to.be.enabled;
}

const expectNotEnabled = function (page,elementSelector){
  return page.expect.element(elementSelector).to.not.be.enabled;
}

var expectNotFound = function(page,elementSelector) {
  return page.expect.element(elementSelector).not.to.be.present;
};

// expect to be visible
const expectVisible = function (page, elementSelector) {
  waitElementVisible(page, elementSelector);
  return page.expect.element(elementSelector).to.be.visible;
};

// expect to not be visible
const expectNotVisible = function (page, elementSelector) {
  return page.expect.element(elementSelector).to.not.be.visible;
};

// choose an option from dropdown list
var chooseOptionValue = function(page,elementSelector,selectedOption){
  waitElementVisible(page, elementSelector);
  return page.click(elementSelector,()=>{
    pauseSleep (page, 2000);
    if(selectedOption=='')
      page.click(elementSelector + ">option[value]");
    else
      page.click(elementSelector + ">option[value='"+selectedOption+"']");
  })
};

// choose an option from dropdown list based on the class
var chooseOptionClass = function(page,elementSelector,value){
  waitElementVisible(page, elementSelector);
  return page.click(elementSelector,()=>{
    if(value==''){
      page.click(elementSelector + ">option[class]");
    }
    else{
      waitElementVisible(page, elementSelector + ">option[class='ts-option-"+value.replace(/\\|\/|\)|\(|\&|\.|,|\s/g,'').toLowerCase()+"']");
      page.click(elementSelector + ">option[class='ts-option-"+value.replace(/\\|\/|\)|\(|\&|\.|,|\s/g,'').toLowerCase()+"']");
    }
  })
};

const expectElementSelected = function (page, elementSelector) {
  return page.expect.element(elementSelector).to.be.selected;
};

var assertElementNotPresent = function (page, elementSelector) {
  return page.waitForElementNotPresent (elementSelector, timeOut);
};

var waitUntilElementEnabled = function(page, elementSelector, timeOut){
  waitElementPresent(page, elementSelector);
  page.waitForElementVisible(elementSelector+':enabled', timeOut);
}

var waitUntilElementDisabled = function(page, elementSelector, timeOut){
  waitElementPresent(page, elementSelector);
  page.waitForElementVisible(elementSelector+':disabled', timeOut);
}

module.exports = {
  clickElement,
  waitElementPresent,
  waitElementVisible,
  setValueElement,
  getStringText,
  assertPageTitle,
  expectEqualsTextFromElement,
  pauseSleep,
  scrollToElement,
  setValueElementThenEnter,
  expectVisible,
  chooseOptionValue,
  assertContainsText,
  expectElementSelected,
  assertElementNotPresent,
  expectNotVisible,
  chooseOptionClass,
  expectEqualsValueFromElement,
  expectEnabled,
  expectNotEnabled,
  expectNotFound,
  waitUntilElementEnabled,
  waitUntilElementDisabled,
};
