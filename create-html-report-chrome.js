const report = require('multiple-cucumber-html-reporter');

report.generate({
  jsonDir: 'reports',
  reportPath: 'reports',
  displayDuration: true,
  openReportInBrowser: true,
  metadata: {
    browser: {
      name: 'chrome',
      version: '68',
    },
    device: 'Local',
    platform: {
      name: 'ubuntu',
      version: '16.04',
    },
  },
  customData: {
    title: 'Run info',
    data: [
      { label: 'Project', value: 'Web Seva - Parent Site' },
      { label: 'Written by', value: 'Astra International QA Team' },
      { label: 'Repo', value: 'astraazure@vs-ssh.visualstudio.com:v3/astraazure/Seva%20Parent%20Site/at-web-seva-parent-site' },
    ],
  },
});
